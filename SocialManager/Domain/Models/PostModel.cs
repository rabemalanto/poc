﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Models
{
    public class PostModel
    {
        public string message { get; set; }
        public string accessToken { get; set; }
    }
}
