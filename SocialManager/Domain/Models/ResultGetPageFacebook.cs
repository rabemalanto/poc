﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Models
{
    public class ResultGetPageFacebook
    {
        public List<Pages> data { get; set; }
        public object paging {get;set;}
    }
}
