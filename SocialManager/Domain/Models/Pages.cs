﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Models
{
    public class Pages
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<string> tasks { get; set; }

    }
}
