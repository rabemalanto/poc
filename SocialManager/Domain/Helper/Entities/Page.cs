﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Helper.Entities
{
    public class Page
    {
        public string id { get; set; }
        public string name { get; set; }
        public string access_token { get; set; }
        public string category { get; set; }
    }
}
