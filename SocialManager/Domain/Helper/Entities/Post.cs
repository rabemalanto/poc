﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Helper.Entities
{
    public class Post
    {
        public string id { get; set; }
        public string message { get; set; }
        public string created_time { get; set; }
    }
}
