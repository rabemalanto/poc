﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rest.Net;
using Rest.Net.Interfaces;
using SocialManager.Domain.Helper.APIs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SocialManager.Domain.Helper
{
    public class LinkedInClient
    {
        private LinkedInAppApi _app;

        internal string ClientId { get; private set; }

        internal string ClientSecret { get; private set; }

        internal IRestClient RestClient { get; private set; }

        public string Version { get; set; } = "v2";

        public LinkedInAppApi App => _app ?? (_app = new LinkedInAppApi(this));

        public LinkedInClient(string clientId, string clientSecret)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
            RestClient = new RestClient("https://api.linkedin.com/");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public async Task<string> GetAccessToken(string code, string redirectUrl)
        {
            string request = $"/oauth/v2/accessToken?code={code}&grant_type=authorization_code&redirect_uri={redirectUrl}&client_id={ClientId}&client_secret={ClientSecret}";
            var json = await GetAsync(request);
            return json["access_token"]?.ToString();
        }

        public async Task<object> GetCompanies(string accessToken)
        {
            string request = $"/companies?is-company-admin=true";
            var json = await GetAsync2(request, accessToken);
            return json;
        }

        public async Task<string> GetUserUrn(string accessToken)
        {
            string request = $"/me";
            var json = await GetAsync2(request, accessToken);
            return "urn:li:person:" + json["id"]?.ToString(); ;
        }

        public async Task<object> CreateShare(string accessToken, string message)
        {
            string request = $"/ugcPosts";
            string author = (await GetUserUrn(accessToken));
            object body = (object)JObject.Parse("{'author': '" + author + "'," +
                                    "'lifecycleState': 'PUBLISHED'," +
                                    "'specificContent': {" +
                                                            "'com.linkedin.ugc.ShareContent': {" +
                                                                        "'shareCommentary': {" +
                                                                                                "'text': '" + message + "'" +
                                                                                            "}," +
                                                                      "'shareMediaCategory': 'NONE'" +
                                                              "}" +
                                                        "}," +
                                    "'visibility': {" +
                                                    "'com.linkedin.ugc.MemberNetworkVisibility': 'PUBLIC'" +
                                                    "}" +
        "}");
            var json = await PostAsync(request, accessToken, body);
            return json;
        }

        // Provide Urn 
        public async Task<string> GetUserHandle(string accessToken)
        {
            string request = $"/emailAddress?q=members&projection=(elements*(handle~))";
            var json = await GetAsync2(request, accessToken);
            return json["elements"].First["handle"].ToString();
        }

        public async Task<JObject> GetAsync(string path, string accessToken = null)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }

            if (accessToken == null)
            {
                accessToken = string.Empty;
            }
            else
            {
                accessToken = (path.Contains("?") ? "&" : "?") + "access_token=" + accessToken;
            }
            try
            {
                string request = $"/{path}{accessToken}";
                var response = await RestClient.GetAsync($"{path}{accessToken}", false);
                var serializedResponse = SerializeResponse(response);
                return serializedResponse;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<JObject> GetAsync2(string path, string accessToken = null)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }

            if (accessToken == null)
            {
                accessToken = string.Empty;
            }
            else
            {
                accessToken = (path.Contains("?") ? "&" : "?") + "oauth2_access_token=" + accessToken;
            }
            try
            {
                string request = $"/{Version}{path}{accessToken}";
                var response = await RestClient.GetAsync($"{Version}{path}{accessToken}", false);
                var serializedResponse = SerializeResponse(response);
                return serializedResponse;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<JObject> PostAsync(string path, string accessToken = null, object body = null)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }
            if (accessToken == null)
            {
                accessToken = string.Empty;
            }
            else
            {
                accessToken = (path.Contains("?") ? "&" : "?") + "oauth2_access_token=" + accessToken;
            }
            try
            {
                var response = await RestClient.PostAsync($"{Version}{path}{accessToken}", body,false);
                var serializedResponse = SerializeResponse(response);
                return serializedResponse;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        internal JObject SerializeResponse(IRestResponse<string> response)
        {
            try
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var jsreader = new JsonTextReader(new StringReader(response.RawData.ToString()));
                    var json = (JObject)new JsonSerializer().Deserialize(jsreader);
                    return json;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
