﻿using FacebookCore;
using FacebookCore.Collections;
using Newtonsoft.Json.Linq;
using SocialManager.Domain.Helper.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Helper.Collections
{
    public class PageCollection : FacebookCollection<Page>
    {
        private FacebookClient _fbClient;
        private string _appAccessToken;
        private string _appId;

        public FacebookCursor Cursor { get; internal set; }

        public PageCollection(FacebookClient client, string query, string token, FacebookCursor cursor = null) : base(client, query, token, TestUserMapper, cursor)
        {

        }

        public new async Task<PageCollection> BeforeAsync()
        {
            FacebookCollection<Page> collection = await base.BeforeAsync();
            return (PageCollection)collection;
        }

        public new async Task<PageCollection> AfterAsync()
        {
            FacebookCollection<Page> collection = await base.AfterAsync();
            return (PageCollection)collection;
        }

        private static Page TestUserMapper(JToken data)
        {
            Page user = new Page()
            {
                access_token = data["access_token"].ToString(),
                id = data["id"].ToString(),
                category = data["category"].ToString(),
                name = data["name"].ToString()
            };

            return user;
        }
    }
}
