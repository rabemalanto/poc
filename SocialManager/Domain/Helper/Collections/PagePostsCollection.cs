﻿using FacebookCore;
using FacebookCore.Collections;
using Newtonsoft.Json.Linq;
using SocialManager.Domain.Helper.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Helper.Collections
{
    public class PagePostsCollection : FacebookCollection<Post>
    {
        private FacebookClient _fbClient;
        private string _appAccessToken;
        private string _appId;

        public FacebookCursor Cursor { get; internal set; }

        public PagePostsCollection(FacebookClient client, string query, string token, FacebookCursor cursor = null) : base(client, query, token, TestUserMapper, cursor)
        {

        }

        public new async Task<PagePostsCollection> BeforeAsync()
        {
            FacebookCollection<Post> collection = await base.BeforeAsync();
            return (PagePostsCollection)collection;
        }

        public new async Task<PagePostsCollection> AfterAsync()
        {
            FacebookCollection<Post> collection = await base.AfterAsync();
            return (PagePostsCollection)collection;
        }

        private static Post TestUserMapper(JToken data)
        {
            Post user = new Post()
            {
                id = data["id"].ToString(),
                created_time = data["created_time"].ToString(),
                message = data["message"].ToString()
            };

            return user;
        }
    }
}
