﻿using SocialManager.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Abstract
{
    public interface ISocialeFactory
    {
        Task<dynamic> GetPageList();
        Task<dynamic> GetPosts(string pageId, string UserAccessToken);
        Task<dynamic> GetPostAsync();
        Task<Tuple<int, string>> PostOnPage(string id, string v);
        Task<object> CreatePagePost(string idPage, string pageAccessToken, string message);
        Task<object> CreateShare(string message);
    }
}
