﻿using SocialManager.Domain.Abstract;
using SocialManager.Domain.Helper.Entities;
using SocialManager.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Concrete
{
    public class SocialeFactory
    {
        private ISocialeFactory sociale;
        public SocialeFactory(ISocialeFactory s)
        {
            sociale = s;
        }

        public async Task<List<Page>> GetAllPages() => (List<Page>)(await sociale.GetPageList());
        public async Task<List<Post>> GetPosts(string id, string userTokenAccess) => (List<Post>)(await sociale.GetPosts(id,userTokenAccess));
        public async Task<object> CreatePostAsync(string idPage, string pageAccessToken, string message)
        {
            return await sociale.CreatePagePost(idPage, pageAccessToken, message);
        }

        public async Task<object> CreateShare(string message)
        {
            return await sociale.CreateShare(message);
        }
    }
}
