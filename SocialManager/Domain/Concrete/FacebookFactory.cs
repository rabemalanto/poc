﻿using FacebookCore;
using FacebookCore.Collections;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SocialManager.Domain.Abstract;
using SocialManager.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SocialManager.Domain.Concrete
{
    public class FacebookFactory : ISocialeFactory
    {
        private FacebookClient client;
        private string UserAccessToken;
        public FacebookFactory(string accessToken, IConfiguration _configuration)
        {
            UserAccessToken = accessToken;
            client = new FacebookClient(_configuration["facebookAppId"], _configuration["facebokkAppSecrete"]);
        }

        public async Task<dynamic> GetPostAsync()
        {
            var r = client.GetAsync("/feed");
            return r;
        }

        public async Task<dynamic> GetPageList()
        {
            var listOfPages = await client.App.GetUserPages(UserAccessToken);
            return listOfPages;
        }

        public async Task<dynamic> GetPosts(string pageId, string UserAccessToken)
        {
            var listOfPosts = await client.App.GetUserPagePosts(pageId, UserAccessToken);
            return listOfPosts;
        }

        public async Task<object> CreatePagePost(string idPage, string pageAccessToken, string message)
        {
            string query = $"/{idPage}/feed?message={message}";
            return await client.PostAsync(query, pageAccessToken);
            
        }

        public Task<object> CreateShare(string message)
        {
            throw new NotImplementedException();
        }

        public Task<Tuple<int, string>> PostOnPage(string id, string v)
        {
            throw new NotImplementedException();
        }

    }
}
