﻿using FacebookCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using SocialManager.Domain.Abstract;
using SocialManager.Domain.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialManager.Domain.Concrete
{
    public class LinkedInFactory : ISocialeFactory
    {
        private LinkedInClient client;
        private string UserAccessToken;
        public LinkedInFactory(string accessToken, IConfiguration configuration)
        {
            client = new LinkedInClient(configuration["linkedInClientID"], configuration["lindedInClientSecret"]);
            UserAccessToken = client.GetAccessToken(accessToken, configuration["linkedInRedirectUrl"]).Result;
        }
        public Task<object> CreateShare(string message)
        {
            return client.CreateShare(UserAccessToken, message);
        }
        public Task<object> CreatePagePost(string idPage, string pageAccessToken, string message)
        {
            throw new NotImplementedException();
        }

        public string getUserAccessToken() => UserAccessToken;

        public Task<dynamic> GetPageList()
        {
            return client.GetCompanies(UserAccessToken);
        }

        public Task<dynamic> GetPostAsync()
        {
            throw new NotImplementedException();
        }

        public Task<dynamic> GetPosts(string pageId, string UserAccessToken)
        {
            throw new NotImplementedException();
        }

        public Task<Tuple<int, string>> PostOnPage(string id, string v)
        {
            throw new NotImplementedException();
        }
    }
}
