﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SocialManager.Domain.Concrete;
using SocialManager.Domain.Helper.Entities;
using SocialManager.Models;

namespace SocialManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration configuration;
        public HomeController(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public ActionResult Index(string code, string state)
        {
            
            if (code != null && state != null)
            {
                HttpContext.Session.SetString("AccessTokenLinkedIn", code);
                HttpContext.Session.SetString("StateLinkedIn", code);
                return RedirectToAction("Index", "LindedIn");
            }
            ViewBag.ClientId = configuration["linkedInClientID"];
            ViewBag.ClientSecret = configuration["lindedInClientSecret"];
            ViewBag.RedirectUrl = configuration["linkedInRedirectUrl"];
            ViewBag.Scope = configuration["linkedInScopePermission"];
            ViewBag.FacebookAppId = configuration["facebookAppId"];
            return View();
        }

        [HttpPost]
        public ActionResult SetAccessToken(string AccessToken)
        {
            HttpContext.Session.SetString("AccessToken", AccessToken);
            return Ok();
        }
        public SocialeFactory GetFacebookFactory()
        {
            string UserAccessToken = HttpContext.Session.GetString("AccessToken");
            return new SocialeFactory(new FacebookFactory(UserAccessToken,configuration));
        }
        public ViewResult Facebook()
        {
            List<Page> pages = GetFacebookFactory().GetAllPages().Result;
            return View(pages);
        }
        public ViewResult ShowFBPage(string pageId, string accessToken, string pageName)
        {
            ViewBag.AccessToken = accessToken;
            ViewBag.IdPage = pageId;
            ViewBag.PageName = pageName;
            List<Post> posts = GetFacebookFactory().GetPosts(pageId, accessToken).Result;
            return View(posts);
        }

        public ActionResult CreatePost(string idPage, string pageAccessToken, string message)
        {
            var result = GetFacebookFactory().CreatePostAsync(idPage, pageAccessToken, message).Result;
            return RedirectToAction("ShowFBPage", new { pageId = idPage, accessToken = pageAccessToken });
        }
       
    }
}
