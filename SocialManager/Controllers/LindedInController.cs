﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SocialManager.Domain.Concrete;

namespace SocialManager.Controllers
{
    public class LindedInController : Controller
    {
        private readonly IConfiguration configuration;
        public LindedInController(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        public ActionResult Index()
        {
            //var pages = GetLinkedInFactory().GetAllPages();
            return View();
        }

        public ActionResult CreateShare(string message)
        {
           var response = GetLinkedInFactory().CreateShare(message).Result;
            return RedirectToAction("Index");
        }

        public SocialeFactory GetLinkedInFactory()
        {
            string UserAccessToken = HttpContext.Session.GetString("AccessTokenLinkedIn");
            return new SocialeFactory(new LinkedInFactory(UserAccessToken, configuration));
        }
    }
}